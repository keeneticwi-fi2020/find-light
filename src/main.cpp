#include <Arduino.h>

//-------------------------------------------------------------------------------------------------
#define SENSOR_COUNT 5
#define BUTTON_PIN 19
#define LEARN_READY_LED_PIN 22
#define LEARN_MODE_LED_PIN 21
long step = 80;
long impulseLeng;
long lastWork = 0;
int lernMode = 0;
int sensorPins[] = {26, 27, 32, 34, 35};
float sensorValues[SENSOR_COUNT];
int buttonState = LOW;
int sensorAngles[SENSOR_COUNT] = {-90, -45, 0, 45, 90};
long lastTimeReadSensors = 0;
bool buttonPressed = false;
int sensorDarkValues[SENSOR_COUNT];
int sensorLightValues[SENSOR_COUNT];

//-----------------------------------------------------------------------------------------------------
void setup() {
    for (int index = 0; index < SENSOR_COUNT; index++) {
        pinMode(sensorPins[index], INPUT);
    }
    Serial.begin(115200);
    pinMode(BUTTON_PIN, INPUT_PULLDOWN);
    pinMode(LEARN_READY_LED_PIN, OUTPUT);
    pinMode(LEARN_MODE_LED_PIN, OUTPUT);
}
//-----------------------------------------------------------------------------------------------------
void calc_angle(){
    //найти максимум
    int index_of_maximum = 0;
    double  maximum = sensorValues[0];

    for (int ind = 1; ind < SENSOR_COUNT; ind++){
        if(maximum <= sensorValues[ind]){
         maximum = sensorValues[ind];
         index_of_maximum = ind;
        }
    }
    //найти второй максимум рядом с первым
    double left_value, right_value;
    if(index_of_maximum == 0){
        left_value = -1;
    } else{
        left_value = sensorValues[index_of_maximum -1];
    }
    if((index_of_maximum + 1) >= SENSOR_COUNT){
        right_value = -1;
    } else {
        right_value = sensorValues[index_of_maximum + 1];
    }


    //найти угол между датчиками, которые имеют два найденных максимуму. Сразу считаем знак угла
    //найти угол по формуле а = dA * max2 /

}
//-----------------------------------------------------------------------------------------------------
void readSensors() {
    for (int index = 0; index < SENSOR_COUNT; index++) {
        int val = analogRead(sensorPins[index]);
        sensorValues[index] = (val - sensorDarkValues[index])/ (sensorLightValues[index] - sensorDarkValues[index]);
    }
    for (int index = 0; index < SENSOR_COUNT; index++) {
        Serial.print(" = " + String(sensorValues[index]));
    }
}

//-----------------------------------------------------------------------------------------------------
void analyzeSensors() {
    if ((lastTimeReadSensors < (millis() - 200))) {
        lastTimeReadSensors = millis();

        readSensors();

        for (int index = 0; index < SENSOR_COUNT; index++) {
            sensorValues[index] = analogRead(sensorPins[index]);
        }
        float max = sensorValues[0];
        int maxind = 0;

        for (int ind = 1; ind < SENSOR_COUNT; ind++) {
            if (max < sensorValues[ind]) {
                max = sensorValues[ind];
                maxind = ind;
            }
        }
        for (int index = 0; index < SENSOR_COUNT; index++) {
            sensorValues[index] = 0;
        }
        sensorValues[maxind] = 1;

        for (int index = 0; index < SENSOR_COUNT; index++) {
            Serial.print(String((int)sensorValues[index]) + "     ");
        }
Serial.println();
    }
}

//-----------------------------------------------------------------------------------------------------
void setLearnLedState() {
    switch (lernMode) {
        case 0 : {
            digitalWrite(LEARN_READY_LED_PIN, LOW);
            digitalWrite(LEARN_MODE_LED_PIN, LOW);
            break;
        }
        case 1 : {
            digitalWrite(LEARN_READY_LED_PIN, HIGH);
            digitalWrite(LEARN_MODE_LED_PIN, LOW);
            break;
        }
        case 2 : {
            digitalWrite(LEARN_READY_LED_PIN, HIGH);
            digitalWrite(LEARN_MODE_LED_PIN, HIGH);
            break;
        }
    }
}

//-----------------------------------------------------------------------------------------------------
void habdleButton() {
    int currentButtonState = digitalRead(BUTTON_PIN);

    if ((buttonState == LOW) && (currentButtonState == HIGH)) {
        buttonState = HIGH;
        buttonPressed = true;
        delay(50);
    } else if ((buttonState == HIGH) && (currentButtonState == LOW)) {
        buttonState = LOW;
        delay(50);
    }

}

//-----------------------------------------------------------------------------------------------------
void learnDark() {
    for (int index = 0; index < SENSOR_COUNT; index++) {
        sensorDarkValues[index]
                = analogRead(sensorPins[index]);
    }
    Serial.println("");
    for (int index = 0; index < SENSOR_COUNT; index++) {
        Serial.println(String(index) + " = " + sensorDarkValues[index]);
    }
}

//-----------------------------------------------------------------------------------------------------
void learnLight() {
    for (int index = 0; index < SENSOR_COUNT; index++) {
        sensorLightValues[index]
                = analogRead(sensorPins[index]);
    }
    Serial.println("");
    for (int index = 0; index < SENSOR_COUNT; index++) {
        Serial.println(String(index) + " = " + sensorLightValues[index]);
    }
}

//--------------------------------------------------------------------------------------------------------
void loop() {
    habdleButton();
    //  readSensors();
    if (buttonPressed) {
        buttonPressed = false;

        if (lernMode == 1) {
            learnDark();
        } else if (lernMode == 2) {
            learnLight();
        }
        lernMode++;
        if (lernMode > 2) {
            lernMode = 0;
        }
    }
        setLearnLedState();
        if (lernMode == 0) {
            analyzeSensors();
        }
    }


//--------------------------------------------------------------------------------------------------------